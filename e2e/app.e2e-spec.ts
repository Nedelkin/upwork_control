import { JqueryUiControlPage } from './app.po';

describe('jquery-ui-control App', function() {
  let page: JqueryUiControlPage;

  beforeEach(() => {
    page = new JqueryUiControlPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
