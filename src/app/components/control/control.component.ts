import {Component, Input} from "@angular/core";
@Component({
  selector: 'up-control',
  template: `
    <h2>Controll</h2>
    <div class="selector-container">
      <div class="left-list">
        <ul>
          <li *ngFor="let control of data">
            <label><input type="checkbox" (click)="checked(control.id)">{{control.name}}</label>
          </li>
        </ul>
      </div>
      <div class="right-list">
          <ul>
            <li *ngFor="let checked_control of checked_data">
              {{checked_control.name}}
            </li>
          </ul>
      </div>
        
    </div>
    
`, styles: [`
  .selector-container {
      border: 1px solid grey;
      width: 100%;
      height: 100%;
      position: relative;
  }
  
  .selector-container .left-list,
  .selector-container .right-list {
      float: left;
      width: 50%;
      height: 100%;
      margin: 0;
      border: 1px solid green;
      box-sizing: border-box;
  }
  
  .selector-container ul {
      list-style: none;
      margin: 0;
      padding: 5px;
      line-height: 1.5;
  }
`]
})
export class UpControlComponent {
  @Input()
  data;

  checked_data = [];

  checked(id:number) {
    let checked_field = this.checked_data.find((obj) => obj.id === id);
    if (checked_field) {
      let index = this.checked_data.indexOf(checked_field);
      this.checked_data.splice(index, 1);
    } else {
      this.checked_data.push(this.data.find((obj) => obj.id === id));
    }
  }
}
