import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';

  data = [
    {id: 1, name: 'Option1'},
    {id: 2, name: 'Option2'},
    {id: 3, name: 'Option3'},
    {id: 4, name: 'Option4'},
    {id: 5, name: 'Option5'},
    {id: 6, name: 'Option6'}
  ];
}
