﻿(function ($) {
    $.fn.getSelected = function () {
        checkLength(this);
        return getValue(detectSingleInstance(this[0]));
    };

    $.fn.setSelected = function (ids) {
        return this.each(function () {
            setValue($(this), detectSingleInstance(this), ids);
        });
    };

    $.fn.itemSelector = function (data) {
        var args = arguments;
        return this.each(function () {
            var el = $(this);
            var selected = {};
            var options = el.data('itemSelectorOptions');
            if (!options) {
                // Create
                options = buildOptions(data);
                build(el, options);
                el.data('itemSelectorOptions', options);
            }

            // Created. Manipulate
        });

        function buildOptions(data) {
            return {
                _data: data,
                _selected: {}
            };
        }

        function renderLeftList(el, options, data) {
            var ul = el.find('.left-list ul');
            ul.empty();

            $.each(data, function () {
                var label = $('<label></label>')
                    .append('<input type="checkbox" />')
                    .append(document.createTextNode(this.name));

                var li = $('<li></li>')
                    .append(label);

                li.find('input')
                    .attr('data-id', this.id)
                    .change(function () {
                        setSelected(el, options, $(this).data('id'), $(this).is(':checked'));
                        renderRightList(el, options);
                    });
                ul.append(li);
            });
        }

        function build(el, options) {
            var data = options._data;
            var container = $('<div class="selector-container"></div>');
            var leftList = $('<div class="left-list"><ul></ul></div>');
            var rightList = $('<div class="right-list"><ul></ul></div>');

            el.append(container);
            container.append(leftList).append(rightList);
            renderLeftList(el, options, data);
        }
    };


    function getValue(options) {
        var result = [];
        var data = options._data;
        var selected = options._selected;

        $.each(data, function () {
            if (selected[this.id]) {
                result.push(this);
            }
        });

        return result;
    }

    function renderRightList(el, options) {
        var ul = el.find('.right-list ul');
        ul.empty();

        var value = getValue(options);
        $.each(value, function () {
            $('<li></li>').text(this.name).attr('data-id', this.id).appendTo(ul);
        });
    }

    function setSelected(el, options, id, value) {
        options._selected[id] = value;
        el.find('[data-id=' + id + ']').prop('checked', value);
    }

    function setValue(el, options, ids) {
        el.find('.left-list input').each(function () {
            var input = $(this);
            var id = parseInt(input.data('id'));
            setSelected(el, options, id, ~ids.indexOf(id));
        });

        renderRightList(el, options);
    }

    function detectSingleInstance(ctx) {
        var options = $(ctx).data('itemSelectorOptions');
        if (!options) {
            throw new Error('Item selector is not initialized. Call itemSelector() first');
        }

        return options;
    }

    function checkLength(ctx) {
        if (!ctx.length) {
            throw new Error('No itemSelector found');
        }

    }
})(jQuery);